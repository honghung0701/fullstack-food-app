import React, { useEffect } from "react";
import Header from "../components/Header";
import Home from "../components/Home";
import HomeSlider from "../components/HomeSlider";
import FilterSection from "../components/FilterSection";
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../api";
import { setAllProducts } from "../context/actions/productAction";
import Cart from "../components/Cart";
import Footer from "../components/Footer";

const Main = () => {
  const products = useSelector((state) => state.products);
  const isCart = useSelector((state) => state.isCart);
  const dispatch = useDispatch;

  useEffect(() => {
    if (!products) {
      getAllProducts().then((data) => {
        dispatch(setAllProducts(data));
      });
    }
  });
  return (
    <main className="w-screen min-h-screen flex items-center justify-start flex-col bg-primary">
      <Header />
      <div className="w-full flex flex-col items-start justify-center mt-40 px-6 md:px-24 2xl:px-96 gap-12 pb-24">
        <Home />
        <HomeSlider />
        <FilterSection />
      </div>
      <div className="w-full  ">
        <Footer />
      </div>

      {isCart && <Cart />}
    </main>
  );
};

export default Main;
