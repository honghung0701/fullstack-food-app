export const isActiveStyles =
  "text-2xl text-red-700 font-semibold hover:text-red-700 px-4 py-2 duration-100 transition-all ease-in-out";

export const isNotActiveStyles =
  "text-xl text-textColor hover:text-red-700 duration-100 px-4 py-2 transition-all ease-in-out";
export const statuses = [
  { id: 1, title: "Drinks", category: "drinks" },
  { id: 2, title: "Deserts", category: "deserts" },
  { id: 3, title: "Fruits", category: "fruits" },
  { id: 4, title: "Rice", category: "rice" },
  { id: 5, title: "Curry", category: "curry" },
  { id: 6, title: "Chinese", category: "chinese" },
  { id: 7, title: "Bread", category: "bread" },
];
export const randomData = [
  {
    id: 1,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684225315132_i1.png?alt=media&token=5b690595-c763-49d5-b83d-af7d532225c8",
    product_category: "deserts",
    product_name: "Ice Cream",
    product_price: "23",
  },
  {
    id: 2,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684225512689_f1.png?alt=media&token=250d1c59-5c58-4a75-bc95-4b959cda3b32",

    product_category: "fruits",
    product_name: "Straw Berry",
    product_price: "18",
  },
  {
    id: 3,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684225564047_f2.png?alt=media&token=826212a6-5bd1-4218-a7c3-c558043f8220",

    product_category: "fruits",
    product_name: "Pine Apple",
    product_price: "16",
  },
  {
    id: 4,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684222126530_c3.png?alt=media&token=34ec70fd-f1d1-4507-bd10-6888c3c844c7",

    product_category: "bread",
    product_name: "Mix Kebab",
    product_price: "33",
  },
  {
    id: 5,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684225584477_d4.png?alt=media&token=9281f4cd-9f5c-4aa8-a316-a74fd8ed4223",

    product_category: "drinks",
    product_name: "Mojito",
    product_price: "10",
  },
  {
    id: 6,
    imageURL:
      "https://firebasestorage.googleapis.com/v0/b/fullstack-food-app-d7867.appspot.com/o/Images%2F1684222030916_c1.png?alt=media&token=b10a4642-92dd-460a-a68c-03214257ffc7",
    product_category: "chinese",
    product_name: "Chicken Manjurian",
    product_price: "20",
  },
];
